//const http = require('http')
const { request, response } = require('express')
const express = require('express')
const app = express()

const notes = [
    {
        "id":1,
        "Content": "Un titulo",
    },
    {
        "id":2,
        "Content": "Un titulo",
    },
    {
        "id":3,
        "Content": "Otro titulo",
    },
    {
        "id":4,
        "Content": "Un titulo mas",
    }
]

let listado = [
    {
        "id":1,
        "Nombre": "Celular",
    },
    {
        "id":2,
        "Nombre": "Notebook",
    },
    {
        "id":3,
        "Nombre": "Reloj",
    },
    {
        "id":4,
        "Nombre": "Radio",
    }
]

/*node sin framework express
const app = http.createServer((request, response) => {
    //application-json para que el servidor interprete los datos como json 
    response.writeHead(200, {'content-type':'application-json'})
    //Invoco a método de JSON para que pase los datos al servidor y este los pueda entender
    response.end(JSON.stringify(notes))
})
*/

app.get('/', (request, response) => {
    response.send('<h1>Página principal</h1>')
})

app.get('/notas/', (request, response) => {
    //no hace falta aclarar que el content es application-json gracias al framework
    response.json(notes)
})

app.get('/productos/', (request, response) => {
    //response.send('<h1>Productos</h1>')
    response.json(listado)

})

// : significa que es dinámico y va a cambiar
app.get('/productos/:id', (request, response) => {
    //recupera los parámetros que vienen del navegador y se transforma a número    
    const id = Number(request.params.id)
    //buscamos el producto en el array y lo guardamos en producto
    const producto = listado.find(elemento => elemento.id === id)
    console.log(producto)
    console.log(id)
    //devolvemos el producto que encontramos al navegador
    response.json(producto)
})

app.delete('/productos/:id', (request, response) => {
    const id = Number(request.params.id)
    listado = listado.filter(elemento => elemento.id !== id)    
    response.send(`Vamos a borrar el producto ${id}`)
})

app.post('/productos', (request, response) => {
    const producto = request.body
    response.json(producto)
})



const port = 3001
app.listen(port)
//comillas francesas para que me tome la constante port como puerto
console.log(`Server is running on port ${port}`)